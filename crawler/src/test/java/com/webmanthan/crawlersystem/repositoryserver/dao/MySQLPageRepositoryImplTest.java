package com.webmanthan.crawlersystem.repositoryserver.dao;

import java.sql.SQLException;

import javax.naming.NamingException;

import org.junit.Test;
import static org.junit.Assert.*;

import com.webmanthan.crawlersystem.entity.Page;

public class MySQLPageRepositoryImplTest {

	@Test
	public void test() throws NamingException, SQLException {
		MySQLPageRepositoryImpl impl = new MySQLPageRepositoryImpl();
		Page p = new Page();
		p.url = "www.flipkart.com";
		p.urlChecksum = new byte[] {};
		impl.addPage(p);
		
		Page p1 = impl.getPage(1);
		assertEquals(1, p1.id);
	}

}
