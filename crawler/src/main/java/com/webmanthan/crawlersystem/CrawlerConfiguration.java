package com.webmanthan.crawlersystem;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loads all the required configurations for crawler
 * @author aruny
 *
 */
public class CrawlerConfiguration extends Properties {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6937690671603446472L;

	private static final Logger LOGGER = LoggerFactory.getLogger(CrawlerConfiguration.class);
	
	public CrawlerConfiguration() {
		
		InputStream crawlerPropertiesStream = getClass().getResourceAsStream("/crawler.properties");
		try {
			LOGGER.debug("Loading crawler configurations");
			load(crawlerPropertiesStream);
		} catch (IOException e) {
			LOGGER.error("Error loading crawler configuration", e);
			System.exit(1);
		}
	}
}
