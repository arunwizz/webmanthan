package com.webmanthan.crawlersystem.network;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpRequest;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webmanthan.crawlersystem.CrawlerConfiguration;
import com.webmanthan.crawlersystem.core.HTTPResponseHandler;

/**
 * This is a network interfacing class, the crawler can submit the request to
 * this class queue. It will in-turn create thread for each request from
 * controlled thread queue.
 * 
 * @author aruny
 * 
 */
public class NetworkFetcher implements Runnable {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(NetworkFetcher.class);
	private ReentrantLock lock = new ReentrantLock();
	private Condition newFetchRequestcondition = lock.newCondition();
	private LinkedBlockingQueue<URL> requestQueue;
	private Map<URL, HTTPResponseHandler> requestHandlerMap;

	private ExecutorService executor;
	
	private final CrawlerConfiguration conf;

	public NetworkFetcher() {
		requestQueue = new LinkedBlockingQueue<URL>();
		requestHandlerMap = new HashMap<URL, HTTPResponseHandler>();
		conf = new CrawlerConfiguration();
		executor = Executors.newFixedThreadPool(Short.parseShort(conf.getProperty("NETWORK_FETCHER_THREAD_COUNT")));
	}

	public void fetch(HttpHost host, HttpRequest request,
			HTTPResponseHandler httpResponseHandler) {
		lock.lock();
		try {
			URL url = new URL(host.getSchemeName(), host.getHostName(), request
					.getRequestLine().getUri());
			requestQueue.add(url);
			requestHandlerMap.put(url, httpResponseHandler);
			newFetchRequestcondition.signal();
		} catch (MalformedURLException e) {
			LOGGER.info(e.getMessage());
		} finally {
			lock.unlock();
		}
	}

	@Override
	public void run() {
		do {
			LOGGER.info("Looking for queue");
			URL url = null;
			lock.lock();
			try {
				url = requestQueue.poll();
				if (url != null) {
					LOGGER.debug("submitting requesting for " + url);
					executor.submit(new LocalHttpClient(new HttpHost(url
							.getHost()), new BasicHttpRequest("GET", url
							.getPath()), requestHandlerMap.get(url)));

				} else {
					LOGGER.trace("Nothing found, going to sleep");
					newFetchRequestcondition.await(Short.parseShort(conf.getProperty("NETWORK_FETCHER_SLEEP_TIME")), TimeUnit.SECONDS);
				}
			} catch (InterruptedException e) {
				LOGGER.error(e.getMessage());
				requestHandlerMap.get(url).failed(e);
			} finally {
				lock.unlock();
			}
		} while (true);
	}

	private class LocalHttpClient implements Runnable {

		private HttpHost host;
		private HttpRequest request;
		private HTTPResponseHandler httpResponseHandler;
		private HttpClient client;

		public LocalHttpClient(HttpHost host, HttpRequest request,
				HTTPResponseHandler httpResponseHandler) {
			this.host = host;
			this.request = request;
			this.httpResponseHandler = httpResponseHandler;

			client = new DefaultHttpClient();
			HttpParams params = client.getParams();
			
			CrawlerConfiguration conf = new CrawlerConfiguration();
			
			// HTTP parameters for the client
			params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, Integer.parseInt(conf.getProperty("NETWORK_SO_TIMEOUT")))
					.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							Integer.parseInt(conf.getProperty("NETWORK_CONNECTION_TIMEOUT")))
					.setIntParameter(CoreConnectionPNames.SOCKET_BUFFER_SIZE,
							Integer.parseInt(conf.getProperty("NETWORK_CONNECTION_TIMEOUT")))
					.setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, Boolean.parseBoolean(conf.getProperty("NETWORK_TCP_NODELAY")))
					.setParameter(CoreProtocolPNames.USER_AGENT,
							conf.getProperty("HTTP_USER_AGENT")).setParameter("From", conf.getProperty("HTTP_FROM"));
		}

		@Override
		public void run() {
			HttpResponse response = null;
			try {
				long st = System.currentTimeMillis();
				response = client.execute(host, request);
				if (response != null) {
					httpResponseHandler.completed(response);
				} else {
					httpResponseHandler.failed(new Exception(
							"Error getting response"));
				}
				LOGGER.info(host + " fetched in " + (System.currentTimeMillis() - st) + " ms.");
			} catch (ClientProtocolException e) {
				LOGGER.error(e.getMessage());
				httpResponseHandler.failed(e);
			} catch (IOException e) {
				LOGGER.error(e.getMessage());
				httpResponseHandler.cancelled();
			}
		}

	}

}
