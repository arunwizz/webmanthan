package com.webmanthan.crawlersystem.repositoryserver.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.webmanthan.crawlersystem.CrawlerConfiguration;
import com.webmanthan.crawlersystem.entity.Page;
import com.webmanthan.crawlersystem.entity.Page.Status;

public class MySQLPageRepositoryImpl implements PageRepositoryDao {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(MySQLPageRepositoryImpl.class);

	private static final CrawlerConfiguration CONFIGURATION = new CrawlerConfiguration();
	private static final ComboPooledDataSource cpds = new ComboPooledDataSource();

	static {

		try {
			cpds.setDriverClass("com.mysql.jdbc.Driver");
		} catch (PropertyVetoException e) {
			LOGGER.error("Error creating connection pool", e);
		}
		// loads the jdbc driver
		cpds.setJdbcUrl(CONFIGURATION.getProperty("PRS_MYSQL_CONN_URL"));
		cpds.setUser(CONFIGURATION.getProperty("PRS_MYSQL_USER"));
		cpds.setPassword(CONFIGURATION.getProperty("PRS_MYSQL_USER"));

		// the settings below are optional -- c3p0 can work with defaults
		// TODO: LOAD FROM CONF
		cpds.setMinPoolSize(5);
		cpds.setAcquireIncrement(5);
		cpds.setMaxPoolSize(20);
	}

	@Override
	public void addPage(Page p) throws SQLException {

		Connection conn = null;
		try {

			// TODO:To handle null values
			conn = cpds.getConnection();
			String query = "insert into page_repository (status, http_status, url, url_checksum, content, content_size, "
					+ "content_checksum, found_at, downloaded_at, download_attempt_count, page_expires) values "
					+ "(?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, Page.Status.FND.toString());
			stmt.setNull(2, Types.INTEGER);
			stmt.setString(3, p.url);
			stmt.setBytes(4, p.urlChecksum);
			stmt.setNull(5, Types.VARCHAR);
			stmt.setInt(6, Types.INTEGER);
			stmt.setNull(7, Types.BINARY);
			stmt.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
			stmt.setNull(9, Types.TIMESTAMP);
			stmt.setNull(10, Types.TINYINT);
			stmt.setNull(11, Types.TIMESTAMP);
			stmt.execute();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	@Override
	public Page getPage(long id) throws SQLException {
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = cpds.getConnection();
			String query = "select * from page_repository where id = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			Page p = null;
			if (rs.next()) {
				p = new Page(id);
				p.content = rs.getString("content");
				p.contentChecksum = rs.getBytes("content_checksum");
				p.contentSize = rs.getLong("content_size");
				p.downloadAttemptCount = rs.getShort("download_attempt_count");
				p.downloadedAt = rs.getLong("downloaded_at");
				p.foundAt = rs.getLong("found_at");
				p.httpStatus = rs.getShort("http_status");
				p.status = Status.valueOf(rs.getString("status"));
				p.url = rs.getString("url");
				p.urlChecksum = rs.getBytes("url_checksum");
			}
			return p;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

	}

	@Override
	public void updatePage(Page p) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deletePage(long id) {
		// TODO Auto-generated method stub

	}

}
