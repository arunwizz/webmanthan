package com.webmanthan.crawlersystem.repositoryserver.dao;

import java.sql.SQLException;

import com.webmanthan.crawlersystem.entity.Page;

public interface PageRepositoryDao {
	
	public void addPage(Page p) throws SQLException;
	
	public Page getPage(long id) throws SQLException;
	
	public void updatePage(Page p) throws SQLException;
	
	public void deletePage(long id) throws SQLException;

}
