package com.webmanthan.crawlersystem.application.pageprocessor;

import java.util.Set;

/**
 * All configuration related to web crawler
 * @author aruny
 *
 */
public class PageProcessorConfiguration {
	
	private String crawledPageLocation;
	private String processedPageLocation;
	private Set<String> seedHostSet;
	private int crawlerListerPort;
	
	
	public int getCrawlerListerPort() {
		return crawlerListerPort;
	}
	public void setCrawlerListerPort(int crawlerListerPort) {
		this.crawlerListerPort = crawlerListerPort;
	}
	public String getCrawledPageLocation() {
		return crawledPageLocation;
	}
	public void setCrawledPageLocation(String crawledPageLocation) {
		this.crawledPageLocation = crawledPageLocation;
	}
	public String getProcessedPageLocation() {
		return processedPageLocation;
	}
	public void setProcessedPageLocation(String processedPageLocation) {
		this.processedPageLocation = processedPageLocation;
	}
	public Set<String> getSeedHostSet() {
		return seedHostSet;
	}
	public void setSeedHostSet(Set<String> seedHostSet) {
		this.seedHostSet = seedHostSet;
	}

}
