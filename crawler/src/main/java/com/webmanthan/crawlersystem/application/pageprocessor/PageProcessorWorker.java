package com.webmanthan.crawlersystem.application.pageprocessor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.webmanthan.crawlersystem.CrawlerConfiguration;
import com.webmanthan.crawlersystem.application.ds.tree.TreeUtil;
import com.webmanthan.crawlersystem.frontier.FrontierWriter;
import com.webmanthan.crawlersystem.utils.CommonUtil;

/**
 * A PageProcessorWorker Thread, to process/parse the downloaded pages.
 * 
 * Following activities will be taken care by each therad
 * 
 * 1. Pull the page content from page_repository
 * 2. Parse and find all the links in a page. 
 * 3. Clean up the page, fetch the embedded urls and
 * send the request to crawler manager. And then store the cleaned-up page into
 * page storage? (hbase? or BTree?) 5. How will it stop, how will it know that
 * crawling a given host is done?
 * 
 * Also sending new url fetch should go a central FrontierWriter class thread,
 * 
 * @author aruny
 * 
 */
public class PageProcessorWorker implements Runnable {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PageProcessorWorker.class);
	private FrontierWriter frontierWriter;

	private final BlockingQueue<Integer> downloadStatusQueue;
	private final CrawlerConfiguration conf;

	public PageProcessorWorker(CrawlerConfiguration conf,
			FrontierWriter frontierWriter, BlockingQueue<Integer> requestQueue
			) {
		this.conf = conf;
		this.frontierWriter = frontierWriter;
		this.downloadStatusQueue = requestQueue;
	}

	@Override
	public void run() {
		LOGGER.info("Waiting for some download complete signal");
		// consumer
		do {
			try {
				Integer pageId = downloadStatusQueue.take();

				HTMLParser htmlParser = new HTMLParser();

				//GET PAGE CONTENT FROM PAGE_REPOSITORY
				String url = null;//TODO:DB
				String content = getPageContent(pageId);
				LOGGER.debug("Processing page {}",
						url);
				Document document = htmlParser
						.parse(content);
				// TODO: update the cleaned page and status
				// extract all the links and send to frontier
				ArrayList<String> requestURLs = parseDocumentForOutgoingLinks(
						url, document);
				for (String requestURL : requestURLs) {
					frontierWriter.write(requestURL);
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
			}
		} while (true);
	}
	
	private String getPageContent(final long pageId) {
		//TODO:
		return null;
	}

	private void updatePageRepository(final String host, final String path,
			final Document document) throws IOException, TransformerException {


	}

	private ArrayList<String> parseDocumentForOutgoingLinks(final String host,
			final Document document) throws MalformedURLException {

		ArrayList<String> requestURLs = new ArrayList<String>();
		try {
		NodeList anchorList = document.getElementsByTagName("a");
		LOGGER.trace("# {}", anchorList.getLength());
		for (int i = 0; i < anchorList.getLength(); i++) {
			Node anchor = anchorList.item(i);
			String embeddedHref = TreeUtil.getAttrValue(anchor, "href");
			if (embeddedHref == null) {
				continue;
			}
			if (embeddedHref.startsWith("/")) {//FIXME: sometime it can start with no /, handle all cases
				requestURLs.add("http://" + host + embeddedHref);
			} else if (embeddedHref.startsWith("http")) {
				// find the host, and make sure host is not outside seed
				// host
//				if (conf.getSeedHostSet().contains(
//						new URL(embeddedHref).getHost())) {
//					requestURLs.add(embeddedHref);
//				} else {
//					LOGGER.trace(
//							"Found request {} outside seed host {}, ignore!",
//							embeddedHref, host);
//					// FIXME: if you want to enable unbounded host, such
//					// check can be added here
//					continue;
//				}
			} else {
				LOGGER.error(
						"Couldn't understand this relative url {} for host {}",
						embeddedHref, host);
				continue;
			}
		}
		} catch (Exception e) {
			LOGGER.error(e.getStackTrace()[0].toString());
		}
		LOGGER.trace("Found {} next out going links for host {}", requestURLs.size(), host);
		return requestURLs;
	}
}
