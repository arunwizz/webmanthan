package com.webmanthan.crawlersystem.application.pageprocessor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webmanthan.crawlersystem.CrawlerConfiguration;
import com.webmanthan.crawlersystem.frontier.FrontierWriter;

/**
 * This is page processor service. 
 * 
 * This class will spawn page processors thread
 * 
 * @author aruny
 * 
 */
public class PageProcessorServer {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PageProcessorServer.class);

	private final BlockingQueue<Integer> pageProcessorRequestQueue;
	public CrawlerConfiguration conf;

	public PageProcessorServer() {
		pageProcessorRequestQueue = new LinkedBlockingQueue<Integer>();
		conf = new CrawlerConfiguration();
	}
	
	public static void main(String argv[]) throws Exception {
		PageProcessorServer pageProcessorServer = new PageProcessorServer();
		pageProcessorServer.start();

		//monitor to control execution of main thread
		final Lock shutdownHook = new ReentrantLock();
		final Condition shutdownRequest = shutdownHook.newCondition();
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			
			@Override
			public void run() {
				shutdownHook.lock();
				shutdownRequest.signal();
				shutdownHook.unlock();
			}
		}));
		
		shutdownHook.lock();
		LOGGER.info("PageProcessorServer Running..");
		shutdownRequest.await();
		LOGGER.info("Received shutdown request, going to shutdown..");
		
	}
	

	private void start() throws IOException {
		LOGGER.info("Starting frontier writer therad");
		FrontierWriter fw = new FrontierWriter();
		Thread fwt = new Thread(fw, "FrontierWriter");
		fwt.setDaemon(true);
		fwt.start();
		LOGGER.info("Started frontier writer thread");

		LOGGER.info("Starting page processor listner thread");
		Thread downloadStatusListnerThread = new Thread(
				new PageProcessorRequestListener(), "PageProcessorListnerThread");
		downloadStatusListnerThread.start();
		LOGGER.info("Started page processor listner thread");
		
		try {
			PageProcessorWorker pageProcessorWorker;
			ThreadGroup tg = new ThreadGroup("PageProcessorWorker-Group");
			Thread t;
			LOGGER.info("Starting worker threads");
			for (int pageProcessorWorkerCount = 0; pageProcessorWorkerCount < Integer.getInteger("PPS_WORKER_COUNT", Runtime.getRuntime().availableProcessors());) {
				pageProcessorWorker = new PageProcessorWorker(conf, fw, pageProcessorRequestQueue);
				t = new Thread(tg, pageProcessorWorker, "page processor -#" + ++pageProcessorWorkerCount);
				t.setDaemon(true);
				t.start();
				LOGGER.info("Started page processor worker #{} thread", pageProcessorWorkerCount+1);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			System.exit(1);

		}
	}

	/**
	 * A listener socket for page processing request
	 * 
	 * @author aruny
	 * 
	 */
	private final class PageProcessorRequestListener implements Runnable {

		private final Logger LOGGER = LoggerFactory
				.getLogger(PageProcessorRequestListener.class);

		@Override
		public void run() {
			ServerSocket sSocket = null;
			Socket cSocket = null;
			try {
				sSocket = new ServerSocket(Integer.parseInt(conf.getProperty("PPS_PORT")));
				do {
					BufferedReader reader = null;

					try {
						LOGGER.trace("Wating for processing request");
						cSocket = sSocket.accept();
						LOGGER.trace("Received some request");

						reader = new BufferedReader(new InputStreamReader(
								cSocket.getInputStream()));
						String message = reader.readLine();
						LOGGER.debug("Received Message {}", message);
						String[] messageSpit = message.split(":");
						if (!"SUCCESS".equals(messageSpit[0])) {
							LOGGER.error("Can't process failed downloaded");
							//TODO: logic to put this request again, can be here
						} else {
							//producer
							pageProcessorRequestQueue.add(Integer.parseInt(message));
							LOGGER.debug("Put for processing by Crawlerette");
						}

						reader.close();
						cSocket.close();

					} catch (IOException e) {
						LOGGER.error(e.getMessage());
						if (reader != null) {
							try {
								reader.close();
							} catch (IOException e1) {
								LOGGER.error(e1.getMessage());
							}
						}
						if (sSocket != null) {
							try {
								sSocket.close();
							} catch (IOException e2) {
								LOGGER.error(e2.getMessage());
							}
						}
						if (cSocket != null) {
							try {
								cSocket.close();
							} catch (IOException e3) {
								LOGGER.error(e3.getMessage());
							}
						}
					} catch (Exception e) {
						LOGGER.error(e.getMessage());
					}
				} while (true);
			} catch (IOException e) {
				LOGGER.error(e.getMessage());
			}
		}
	}
}
