package com.webmanthan.crawlersystem.entity;


/**
 * Class representing a Web page and all its attributes.
 * 
 * @author aruny
 * 
 */
public class Page {

    public static enum Status {
        FND, TOBE_FETCHED, FETCHED, FAILED_FETCH, RETRY_FETCH
    };

    public long id;
    public Status status;
    public short httpStatus;
    public String url;
    public byte[] urlChecksum;
    public String content;
    public long contentSize;
    public byte[] contentChecksum;
    public long foundAt;
    public long downloadedAt;
    public short downloadAttemptCount;
    
    public long frontierInTime;
    public long frontierOutTime;
    public long downLoadQInTime;
    public long downLoadQOutTime;
    public long networkQInTime;
    public long networkQOutTime;
    public long downLoadStartTime;
    public long downLoadEndTime;
    public long parseStartTime;
    public long parseEndTime;
    
    public Page() {
	}
    
    public Page(long id) {
    	this.id = id;
    }
    
    

}