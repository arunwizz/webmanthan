package com.webmanthan.webbase.index;

public final class IndexElement<K extends Comparable<K>, V> implements
		Comparable<IndexElement<K, V>> {
	private final K key;
	private final V value;

	public IndexElement(final K key, final V value) {
		this.key = key;
		this.value = value;
	}


	K getKey() {
		return key;
	}

	V getValue() {
		return value;
	}

	@Override
	public int compareTo(IndexElement<K, V> o) {
		return key.compareTo(o.key);
	}
	
	@Override
	public String toString() {
		return "[" + key.toString() + ", " + value.toString() + "]";
	}
}
