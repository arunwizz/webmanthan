package com.webmanthan.webbase.index;

import java.util.ArrayList;

import com.google.gson.Gson;

public final class BTreeNode<K extends Comparable<K>, V> {
	
	/* Implementation specific attributes */
	private final String nodePageName;

	/* B-Tree attributes*/
	class NodeAttributes {
		ArrayList<IndexElement<K, V>> indexElements = new ArrayList<IndexElement<K,V>>();
		ArrayList<String> children = new ArrayList<String>();
		boolean leaf;
	};

	NodeAttributes attributes;
	
	public BTreeNode(final String nodePagePath) throws Exception {
		this.nodePageName = nodePagePath;
		this.attributes = new NodeAttributes();
	}
	
	public String getNodePageName() {
		return nodePageName;
	}
	
	K getKey(final int index) {
		return attributes.indexElements.get(index).getKey();
	}

	V getValue(final int index) {
		return attributes.indexElements.get(index).getValue();
	}

	boolean isLeaf() {
		return attributes.leaf;
	}

	void setLeaf(final boolean leaf) {
		this.attributes.leaf = leaf;
	}

	IndexElement<K, V> getKeyValue(final int index) {
		return attributes.indexElements.get(index);
	}

	void addKeyValue(final IndexElement<K, V> keyValue) {
		attributes.indexElements.add(keyValue);
	}

	void addKeyValue(final int index, final IndexElement<K, V> keyValue) {
		attributes.indexElements.add(index, keyValue);
	}

	int getKeySize() {
		return attributes.indexElements.size();
	}

	String getChild(final int index) throws Exception {
		return attributes.children.get(index);
	}

	void addChild(final String childNodePageName) {
		attributes.children.add(childNodePageName);
	}

	void addChild(final int index, final String childNodePageName) {
		attributes.children.add(index, childNodePageName);
	}
	
	public String removeChild(final int minimumDegree) {
		return attributes.children.remove(minimumDegree);
	}	

	public IndexElement<K, V> removeIndexElement(int index) {
		return attributes.indexElements.remove(index);
	}	
	
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(attributes);
	}

}
