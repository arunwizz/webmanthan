package com.webmanthan.webbase.index;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * This is a B-Tree implementation from chapter 'B-Tree' in
 * "Introduction to Algorithm" book
 * 
 * @author aruny
 * 
 */
public class BTree<K extends Comparable<K>, V> {

	private final String NAME;
	private final int MIN_DEGREE;
	private final String STORAGE_DIRECTORY_PATH;

	private BTreeNode<K, V> root;
	final File indexLocation;

	int nodeCount;

	public BTree(final String storageDirectoryPath, final String name,
			final int minDegree) throws Exception {

		this.STORAGE_DIRECTORY_PATH = storageDirectoryPath;
		this.NAME = name;
		this.MIN_DEGREE = minDegree;

		File storageDirectory = new File(STORAGE_DIRECTORY_PATH);
		if (!storageDirectory.exists() || !storageDirectory.isDirectory()) {
			throw new RuntimeException("Storage directory is invalid: "
					+ storageDirectoryPath);
		}

		// CREATE THIS B-TREE INDEX PATH
		indexLocation = new File(STORAGE_DIRECTORY_PATH + File.separator + "_"
				+ NAME + "_idx");
		indexLocation.mkdir();

		// CHECK IF INDEX TO ROOT PAGE ALREADY EXISTS
		File rootPageIndex = new File(indexLocation.getAbsolutePath()
				+ File.separator + "_r");
		if (rootPageIndex.exists()) {/*
									 * IF INDEX TO ROOT EXISTS, LOAD EXISTING
									 * B-TREE
									 */
			BufferedReader reader = new BufferedReader(new FileReader(
					rootPageIndex));
			File rootPage = new File(indexLocation.getAbsolutePath()
					+ File.separator + reader.readLine());
			reader.close();
			setRoot(diskRead(rootPage.getName()));
		} else {/* IF NO INDEX TO ROOT EXISTS CREATE NEW B-TREE */
			BTreeNode<K, V> newRoot = new BTreeNode<K, V>(allocateNode());
			newRoot.setLeaf(true);
			diskWrite(newRoot);
			setRoot(newRoot);
		}
	}

	public BTreeNode<K, V> getRoot() {
		return root;
	}

	public IndexElement<K, V> search(BTreeNode<K, V> node, K key)
			throws Exception {
		int index = 0;
		while (index < node.getKeySize()
				&& key.compareTo(node.getKey(index)) > 0) {
			index++;
		}
		if (index < node.getKeySize() && key.compareTo(node.getKey(index)) == 0) {
			return node.getKeyValue(index);
		} else if (node.isLeaf()) {
			return null;
		} else {
			BTreeNode<K, V> childNode = diskRead(node.getChild(index));
			return search(childNode, key);
		}
	}

	public void insert(BTreeNode<K, V> node, IndexElement<K, V> keyValue)
			throws Exception {
		BTreeNode<K, V> oldRoot = this.root;
		if (oldRoot.getKeySize() == 2 * MIN_DEGREE - 1) {
			BTreeNode<K, V> newRoot = new BTreeNode<K, V>(allocateNode());
			setRoot(newRoot);
			newRoot.setLeaf(false);
			newRoot.addChild(oldRoot.getNodePageName());
			splitChild(newRoot, 0);
			insertNonFull(newRoot, keyValue);
		} else {
			insertNonFull(oldRoot, keyValue);
		}
	}

	private void setRoot(BTreeNode<K, V> newRoot) throws Exception {
		File rootPage = new File(indexLocation + File.separator + "_r");
		BufferedWriter write = new BufferedWriter(new FileWriter(rootPage));
		write.write(newRoot.getNodePageName());
		this.root = newRoot;
		write.close();
	}

	private void splitChild(BTreeNode<K, V> node, int index) throws Exception {
		BTreeNode<K, V> newChildNode1 = diskRead(node.getChild(index));
		BTreeNode<K, V> newChildNode2 = new BTreeNode<K, V>(allocateNode());

		newChildNode2.setLeaf(newChildNode1.isLeaf());
		for (int j = 0; j < (MIN_DEGREE - 1); j++) {
			newChildNode2.addKeyValue(newChildNode1.removeIndexElement(j
					+ MIN_DEGREE));
		}

		if (!newChildNode1.isLeaf()) {
			for (int j = 0; j < MIN_DEGREE; j++) {
				newChildNode2.addChild(newChildNode1
						.removeChild(j + MIN_DEGREE));
			}
		}

		node.addChild(index + 1, newChildNode2.getNodePageName());
		node.addKeyValue(index, newChildNode1.removeIndexElement(MIN_DEGREE - 1));

		diskWrite(newChildNode1);
		diskWrite(newChildNode2);
		diskWrite(node);
	}

	private void insertNonFull(BTreeNode<K, V> node,
			IndexElement<K, V> keyValue) throws Exception {
		int index = node.getKeySize() - 1;
		if (node.isLeaf()) {
			while (index >= 0
					&& keyValue.compareTo(node.getKeyValue(index)) < 0) {
				index--;
			}
			node.addKeyValue(index + 1, keyValue);
			diskWrite(node);
		} else {
			while (index >= 0
					&& keyValue.compareTo(node.getKeyValue(index)) < 0) {
				index--;
			}
			index++;
			BTreeNode<K, V> childNode = diskRead(node.getChild(index));
			if (childNode.getKeySize() == 2 * MIN_DEGREE - 1) {
				splitChild(node, index);
				if (keyValue.compareTo(node.getKeyValue(index)) > 0) {
					index++;
				}
			}
			insertNonFull(childNode, keyValue);
		}
	}

	/**
	 * Thread unsafe method, so be careful!!
	 * 
	 * @return
	 * @throws IOException
	 */
	public String allocateNode() throws IOException {
		File nodePage = new File(indexLocation.getAbsolutePath()
				+ File.separator + "_" + this.nodeCount++);
		nodePage.createNewFile();
		return nodePage.getName();
	}

	/**
	 * Assumes the page content is in one line
	 * 
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	private BTreeNode<K, V> diskRead(final String nodePageName)
			throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(indexLocation + File.separator + nodePageName));
		String pageJSON = reader.readLine();
		BTreeNode<K, V> node = new BTreeNode<K, V>(nodePageName);
		node.attributes = fromJSON(pageJSON);
		reader.close();
		return node;
	}

	private BTreeNode<K, V>.NodeAttributes fromJSON(String pageJSON) {
		Gson gson = new Gson();
		Type nodeAttributeType = new TypeToken<BTreeNode<Integer, V>.NodeAttributes>(){}.getType();
		BTreeNode<K, V>.NodeAttributes attributes = gson.fromJson(pageJSON, nodeAttributeType);
		return attributes;
	}

	/**
	 * Writes current node into corresponding page
	 * 
	 * @throws IOException
	 */
	void diskWrite(BTreeNode<K, V> node) throws IOException {
		FileWriter write = new FileWriter(indexLocation + File.separator + node.getNodePageName());
		write.write(this.toJSON(node));
		write.flush();
		write.close();
	}

	private String toJSON(BTreeNode<K, V> node) {
		Gson gson = new Gson();
		return gson.toJson(node.attributes);
	}
}
