package com.webmanthan.crawlersystem.repository;

public class WebBaseServerConfiguration {
    
    public int PORT = 54000;
    public String WEB_BASE_STORAGE_PATH;
    public short WEB_BASE_BUFFER_THRESHOLD = 1;

}
