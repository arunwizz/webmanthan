package com.webmanthan.crawlersystem.repository;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Arrays;


/**
 * Class representing WebBase Key. It accepts
 * byte[] as a constructor
 * 
 * This class is immutable
 * 
 * @author Arun_Yadav
 *
 */
public class WebBaseKey implements Externalizable {
    private final byte[] idBytes;

    public WebBaseKey(byte[] idBytes) {
        if (idBytes == null) {
            throw new NullPointerException();
        }
        this.idBytes = idBytes;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof WebBaseKey)) {
            return false;
        }
        return Arrays.equals(idBytes, ((WebBaseKey) other).idBytes);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(idBytes);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.write(idBytes);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException,
            ClassNotFoundException {
        in.read(idBytes);
        
    }
    
    @Override
    public String toString() {
        return new String(idBytes);
    }

}
