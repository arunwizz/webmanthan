package com.webmanthan.crawlersystem.repository;


public class WebBaseServer {

//    private final static Logger LOGGER = LoggerFactory
//            .getLogger(WebBaseServer.class);
//
//    private String __STORAGE;
//    private String __INDEX;
//    private String __KEY_COUNT;
//    private WebBaseServerConfiguration configuration;
//
////    private ConcurrentLinkedQueue<Page> webBaseBuffer;
//
//    // IN-MEMORY INDEX
//    private Map<WebBaseKey, Object[]> inMemoryIndex;
//    // PERSISTENT STORAGE
//    private FileChannel pageRepositoryChannel;
//    private RandomAccessFile persistentStorageFile;
//    ObjectOutputStream persistentStorageOutputStream;
//    
//    
//    private ObjectOutputStream indexObjectOutputStream;
//    private ObjectOutputStream keyCountObjectOutputStream;
//    
//    
//    private static final WebBaseServer webBaseServerInstance = new WebBaseServer();
//
//    private WebBaseServer() {
//        
//    }
//    
//    public static final WebBaseServer getInstance(WebBaseServerConfiguration configuration) {
//        webBaseServerInstance.__STORAGE = configuration.WEB_BASE_STORAGE_PATH + "/__data";
//        webBaseServerInstance.__INDEX = configuration.WEB_BASE_STORAGE_PATH + "/__idx";
//        webBaseServerInstance.__KEY_COUNT = configuration.WEB_BASE_STORAGE_PATH
//                + "/__count";
//        webBaseServerInstance.configuration = configuration;
//        return webBaseServerInstance;
//    }
//
//    public void start() throws IOException, ClassNotFoundException {
//
//        //register the shutdown hook
//        Runtime.getRuntime().addShutdownHook(new Thread() {
//            
//            @Override
//            public void run() {
//                LOGGER.info("Finishing shutdown hook..");
//                try {
//                    closeStreams();
//                    LOGGER.info("Finished shutdown hook..");
//                } catch (IOException e) {
//                    LOGGER.error("Error occured during shutdown hook", e);
//                }
//            }
//        });
//        
//        // load
//        inMemoryIndex = new HashMap<WebBaseKey, Object[]>();
//
//        indexObjectOutputStream = new ObjectOutputStream(new FileOutputStream(__INDEX, true));
//        keyCountObjectOutputStream = new ObjectOutputStream(new FileOutputStream(__KEY_COUNT));
//        
//        persistentStorageFile = new RandomAccessFile(__STORAGE, "rw");
//        pageRepositoryChannel = persistentStorageFile
//                .getChannel();
//        persistentStorageOutputStream = new ObjectOutputStream(
//                Channels.newOutputStream(pageRepositoryChannel));
//        
//        startBufferManager();
//
//        // chk if log already exists, then load index
//        initializeIndex();
//
//    }
//    
//    public final void shutdown() throws IOException {
//        LOGGER.info("Shutdown request received");
//        synchronized (this) {
//            notify();
//        }
//        System.exit(0);
//    }
//    
//    private void closeStreams() throws IOException {
//        
//        if (indexObjectOutputStream != null) {
//            indexObjectOutputStream.close();
//        }
//        if (keyCountObjectOutputStream != null) {
//            keyCountObjectOutputStream.close();
//        }
//        if (persistentStorageFile != null) {
//            persistentStorageFile.close();
//        }
//    }
//
//    private void startBufferManager() {
//        webBaseBuffer = new ConcurrentLinkedQueue<Page>();
//        new Thread(new Runnable() {
//
//            // counter to track io error
//            private int ioErrorCount = 0;
//
//            @Override
//            public void run() {
//                synchronized (webBaseBuffer) {
//                    while (true) {
//                        Page webDocument = null;
//                        try {
//                            if (webBaseBuffer.size() < configuration.WEB_BASE_BUFFER_THRESHOLD) {
//                                webBaseBuffer.wait();
//                            }
//                            // DO THE SPILLING
//                            while ((webDocument = webBaseBuffer.poll()) != null) {
//                                writeToPersistentStorage(webDocument);
//                                ioErrorCount = 0;
//                            }
//                        } catch (InterruptedException e) {
//                            LOGGER.warn("Interrupted while waiting!!", e);
//                        } catch (IOException e) {
//                            ioErrorCount++;
//                            LOGGER.warn("Error writing to persistent storage!",
//                                    e);
//                            if (ioErrorCount > 3) {
//                                LOGGER.error("Unable to write to persistent system");
//                                LOGGER.error("Stopping WebBaseServer");
//                                System.exit(1);
//                            }
//                            LOGGER.warn("Adding last polled document back to buffer");
//                            webBaseBuffer.add(webDocument);
//                        }
//                    }
//                }
//            }
//        }, "BufferManager").start();
//    }
//
//    public static void main(String argv[]) {
//        LOGGER.info("Starting WebBase Server..");
//        
//        WebBaseServerConfiguration configuration = new WebBaseServerConfiguration();
//        //TODO: populate it with arguments
//        
//        try {
//            WebBaseServer webBaseServerInstance = WebBaseServer.getInstance(configuration);
//            webBaseServerInstance.start();
//            // start the TCP listener for server request
//            webBaseServerInstance.startServiceListner();
//            LOGGER.info("Server started @ port {}", configuration.PORT);
//            
//            synchronized (webBaseServerInstance) {
//                webBaseServerInstance.wait();
//            }
//            
//        } catch (Throwable t) {
//            LOGGER.error("Error starting server", t);
//            LOGGER.error("Shutting down server...");
//            System.exit(1);
//        } 
//    }
//
//    /**
//     * This will start the TCP/IP listener over network
//     */
//    private void startServiceListner() {
//        new Thread(new Runnable() {
//
//            private final ExecutorService serviceHandler = Executors
//                    .newCachedThreadPool();
//
//            final class RequestHandler implements Runnable {
//
//                private Socket clientSocket;
//
//                public RequestHandler(Socket clientSocket) {
//                    this.clientSocket = clientSocket;
//                }
//
//                @Override
//                public void run() {
//                    Writer responseWriter = null;
//                    BufferedReader requestReader = null;
//                    try {
//                        responseWriter = new BufferedWriter(
//                                new OutputStreamWriter(clientSocket
//                                        .getOutputStream()));
//                        requestReader = new BufferedReader(
//                                new InputStreamReader(clientSocket
//                                        .getInputStream()));
//                        // TODO: define the protocol
//                        // ADD\r\n
//                        // WebDocument as protocol buffer bytes \r\n
//                        // GET\r\n
//                        // DOC_ID\r\n
//                        // CURRENTLY NO UPDATE AND DELETE SUPPORTED
//                        String requestType = requestReader.readLine();
//                        if (requestType == null) {
//                            responseWriter.write("BAD_REQUEST_TYPE");
//                        } else {
//                            String requestContent = requestReader.readLine();
//                            if (requestContent == null) {
//                                responseWriter.write("BAD_REQUEST_CONTENT");
//                            } else if ("ADD".equalsIgnoreCase(requestType)) {
//                                // CONVERT requestContent into WebDocument
//                                Page webDocument = null;// TODO: read it
//                                addWebDocument(webDocument);
//                            } else if ("GET".equalsIgnoreCase(requestType)) {
//                                // read docId and load content from storage and
//                                // return WebDocument as protocol buffer
//                                byte[] documentId = null;// TODO
//                                Page webDocument = null;// getDocument(documentId);
//                                // TODO: convert webdocument into pb bytes
//                                byte[] wdbytes = null;
//                                clientSocket.getOutputStream().write(wdbytes);
//
//                            } else {
//                                responseWriter
//                                        .write("UNSUPPORTED REQUEST TYPE");
//                            }
//                            responseWriter.flush();
//                        }
//
//                    } catch (IOException e) {
//                        LOGGER.error(e.getMessage());
//                    } finally {
//                        try {
//                            if (requestReader != null) {
//                                requestReader.close();
//                            }
//                            if (responseWriter != null) {
//                                responseWriter.close();
//                            }
//                        } catch (IOException e) {
//                            LOGGER.error(e.getMessage());
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public void run() {
//                ServerSocket serverSocket = null;
//                while (true) {
//                    try {
//                        // FIXME: should check for earlier crash log if
//                        // recovering
//                        serverSocket = new ServerSocket(configuration.PORT);
//                        while (true) {
//                            Socket socket = serverSocket.accept();
//                            serviceHandler.execute(new RequestHandler(socket));
//                        }
//
//                    } catch (IOException e) {
//                        LOGGER.error(e.getMessage());
//                        try {
//                            if (serverSocket != null) {
//                                serverSocket.close();
//                            }
//                        } catch (IOException e1) {
//                            LOGGER.error(e.getMessage());
//                        }
//                    }
//                }
//            }
//        }).start();
//    }
//
//    /**
//     * 
//     * @throws IOException
//     * @throws ClassNotFoundException
//     */
//    private void initializeIndex() throws IOException, ClassNotFoundException {
//
//        if (new File(__INDEX).exists()) {
//            ObjectInputStream keyCounterInputStream = new ObjectInputStream(
//                    new FileInputStream(__KEY_COUNT));
//            ObjectInputStream persistentIndexInputStream = null;
//            try {
//                LOGGER.debug("Found existing index, will load it");
//
//                long keyCount = keyCounterInputStream.readLong();
//                if (keyCount > 0) {
//                    persistentIndexInputStream = new ObjectInputStream(
//                            new FileInputStream(__INDEX));
//                    int keyCounter = 0;
//                    while (keyCounter < keyCount) {
//                        WebBaseKey webBaseKey = (WebBaseKey) persistentIndexInputStream
//                                .readObject();
//                        long position = persistentIndexInputStream.readLong();
//                        long length = persistentIndexInputStream.readLong();
//                        inMemoryIndex.put(webBaseKey, new Object[] { position,
//                                length });
//                    }
//                }
//            } catch (EOFException eof) {
//                LOGGER.warn("EOF received, means no key count available");
//            }
//            finally {
//                if (keyCounterInputStream != null) {
//                    keyCounterInputStream.close();
//                }
//                if (persistentIndexInputStream != null) {
//                    persistentIndexInputStream.close();
//                }
//            }
//        }
//
//    }
//
//    public void addWebDocument(Page webDocument) throws IOException {
//        webBaseBuffer.add(webDocument);
//        if (webBaseBuffer.size() >= configuration.WEB_BASE_BUFFER_THRESHOLD) {
//            synchronized (webBaseBuffer) {
//                webBaseBuffer.notify();
//            }
//        }
//    }
//
//    public Page getWebDocument(byte[] documentId)
//            throws NoSuchAlgorithmException, IOException {
//        FileChannel pageRepositoryChannel = persistentStorageFile.getChannel();
//        Object[] value = inMemoryIndex.get(new String(documentId, "UTF8"));
//        Page webDocument = new Page();
//        ByteBuffer byteBuffer = ByteBuffer.allocate((Integer) value[1]);
//        pageRepositoryChannel.read(byteBuffer, ((Long) value[0]).longValue());
////        webDocument.compressedContent = byteBuffer.array();
//        return webDocument;
//    }
//
//    /**
//     * Not a thread safe method
//     * @param webDocument
//     * @throws IOException
//     */
//    private void writeToPersistentStorage(Page webDocument)
//            throws IOException {
//        if (!inMemoryIndex.containsKey(webDocument.id)) {
//
//            long position = pageRepositoryChannel.position();
//            LOGGER.debug("Current Byte Position: {}", position);
//
//            persistentStorageOutputStream.writeObject(webDocument);
//
//            long newPosition = pageRepositoryChannel.position();
//
//            long bytesWritten = newPosition - position + 1;
//            LOGGER.debug("Wrote {} bytes into repository channel", bytesWritten);
//
//            indexObjectOutputStream.writeObject(webDocument
//                    .id);
//            indexObjectOutputStream.writeLong(newPosition + 1);
//            indexObjectOutputStream.writeLong(bytesWritten);
//
////            inMemoryIndex.put(webDocument.id, new Object[] {position, bytesWritten});
//            indexObjectOutputStream.flush();
//            keyCountObjectOutputStream.writeLong(inMemoryIndex.size());
//
//        } else {
//            LOGGER.warn("webdocument {} alread added, ignoring this addition",
//                    webDocument.id);
//        }
//    }

}
