'''
Created on Jul 7, 2012

This is a B-Tree implementation from chapter 'B-Tree' 
in "Introduction to Algorithm" book

@author: अरुण यादव / 阿伦·亚达夫/ Арун Ядав / アルンダヴ / أرون ياداف
'''
###############################################################################

import cPickle, threading
from os import path

class BTree:
    '''
    class representing a BTree
    '''
    
    def __init__(self, min_degree, disk_path):
        self.min_degree = min_degree
        self.disk_path = disk_path
        
        #TODO: if disk_path contains _1 file, load the BTree from this
        
        self.disk_page_counter = 0

        root = self.__allocate_node()
        root.leaf = True
        root.keys = []
        root.write()
        self.root = root
        
            
    def __allocate_node(self): 
        '''allocates a disk page and returns a page '''
        lock = threading.Lock()#simple Lock
        lock.acquire()
        self.disk_page_counter += 1
        node_file = path.join(self.disk_path, '_' + str(self.disk_page_counter))
        lock.release()
        return BTreeNode(path.join(self.disk_path, node_file))
        
            
    def __split_child(self, parent, child_index, child):
        '''splits the child at child_index of a given parent'''
        new_child = self.__allocate_node()
        new_child.is_leaf = True
        for j in range((self.min_degree - 1) - 1):
            new_child.keys.append(child.keys.pop([j + self.min_degree]))
        if not child.is_leaf:
            for j in range(self.min_degree):
                new_child.children.append(child.children.pop([j + self.min_degree]))
        parent.children.append(None)#make space for a new child
        for j in range (len(parent.keys) + 1, child_index + 1, -1):
            parent.children[j+1] = parent.children[j]
        parent.children[child_index + 1] = new_child.disk_path
        parent.keys.append(None)#make space for a new key
        for j in range(len(parent.keys) - 1, child_index, -1):
            parent.keys[j+1] = parent.keys[j]
        parent.keys[child_index] = child.keys.pop(self.min_degree - 1)
        child.write()
        new_child.write()
        parent.write()
          
    
    def __insert_nonfull(self, node, key):
        '''Inserts a given key into a non-full node'''
        i = node.size()
        if node.is_leaf:
            node.keys.append(None)#make space for a new key
            while (i-1) >= 0 and key.compare(node.keys[i-1]) < 0:
                #keep shifting key to right
                node.keys[i] = node.keys[i-1]
                i = i - 1
            node.keys[i] = key
            node.write() 
        else:
            while (i-1) >= 0 and key.compare(node.keys[i-1]) < 0:
                i = i - 1
            child = node.child[i]
            if child.size() == 2*self.min_degree - 1: #CHILD-FULL
                self.__split_child(node, i, child)
                if key.compare(node.keys[i]) > 0:
                    i = i + 1
            self.__insert_nonfull(node.child(i), key)
            

    def insert(self, key):
        '''Inserts a given key into BTree'''
        root = self.root
        if root.size() == self.min_degree:
            new_root = self.__allocate_node()
            self.root = new_root
            new_root.is_leaf = False
            new_root.children.append(root)
            self.__split_child(new_root, 1, root)
            self.__insert_nonfull(new_root, key)
        else:
            self.__insert_nonfull(root, key)
            
    def search(self, start_node, key):
        '''search the given key starting from give start_node'''
        i = 0
        while i < len(start_node.keys) and key.compare(start_node.keys[i]) > 0:
            i = i + 1
        if i < len(start_node.keys) and key.compare(start_node.keys[i]) == 0:
            return (start_node, i)
        if start_node.is_leaf:
            return None
        else:
            self.search(start_node.child(i), key)            
            
###############################################################################

        
###############################################################################        
class BTreeNode:
    '''
    class representing a BTree node
    '''
    
    def __init__(self, disk_path):
        self.is_leaf = True
        self.keys = []
        self.children = []
        self.disk_path = disk_path
        #TODO: allocate a disk page and store the page reference
    
    def child(self, child_index):
        '''Returns child at child_index, 
           would load from disk if not in primary memory
        '''

    def size(self):
        return len(self.keys)

    def write(self):
        '''write this node to file system'''
        #TODO: can we check if something has changed since last dump
        f = open(self.disk_path, 'wb')
        cPickle.dump(self, f, protocol=2)
        f.close()
        
    def read(self):
        '''reads this node from disk block'''
        cPickle.load(self.disk_path)
        
    
        
###############################################################################
    
###############################################################################    
class BTreeNodeKey:
    '''
    class representing a key in a given BTreeNode
    '''
    
    def __init__(self, key, value):
        self.key = key
        self.value = value
        
    def compare(self, other_key):
        if self.key > other_key.key:
            return +1
        elif self.key < other_key.key:
            return -1
        else:
            return 0
            
        
###############################################################################


###############################################################################

    
if __name__ == "__main__":
    BT = BTree(3, '/data/crawler_system/page_repository')
    BTNKey = BTreeNodeKey(1, '<html></html>')
    BT.insert(BTNKey)
    (BTN, I) = BT.search(BT.root, BTNKey)
    print BTN.keys[I].value
    
