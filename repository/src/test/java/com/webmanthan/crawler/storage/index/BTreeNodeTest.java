package com.webmanthan.crawler.storage.index;

import org.junit.Test;

import com.webmanthan.webbase.index.BTree;
import com.webmanthan.webbase.index.BTreeNode;

public class BTreeNodeTest {

	@Test
	public void testBTreeNodeBTreeOfKV() throws Exception {
		BTree<Integer, String> bTree = new BTree<Integer, String>("src/test", "test", 2);
		BTreeNode<Integer, String> node = new BTreeNode<Integer, String>(bTree.allocateNode());
		System.out.print(node);
		
	}

}
