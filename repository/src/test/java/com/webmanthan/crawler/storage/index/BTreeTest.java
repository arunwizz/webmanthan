package com.webmanthan.crawler.storage.index;

import org.junit.Test;

import com.webmanthan.webbase.index.BTree;
import com.webmanthan.webbase.index.IndexElement;

public class BTreeTest {

	@Test
	public void testBTree() throws Exception {
		BTree<String, String> bTree = new BTree<String, String>("src/test", "test", 2);
		bTree.insert(bTree.getRoot(), new IndexElement<String, String>("1", "www.webmanthan.com"));
		bTree.insert(bTree.getRoot(), new IndexElement<String, String>("2", "blogs.webmanthan.com"));
		bTree.insert(bTree.getRoot(), new IndexElement<String, String>("3", "git.webmanthan.com"));
		bTree.insert(bTree.getRoot(), new IndexElement<String, String>("4", "webmanthan.com"));
		
		IndexElement<String, String> keyValue = bTree.search(bTree.getRoot(), "2");
		System.out.print(keyValue.toString());
	}

}
