from scrapy.spider import BaseSpider
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.loader import XPathItemLoader
from scrapy.selector import HtmlXPathSelector
from webmanthan.items import Product
from urlparse import urljoin, urlparse, parse_qs
from scrapy.http import Request
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor

class WebmanthanSpider(CrawlSpider):
    name = "webmanthan"
    allowed_domains = ["flipkart.com"]
    start_urls = [
      "http://www.flipkart.com/"
    ]

    rules = [
            #Rule(SgmlLinkExtractor(allow=(), deny=(r'/sc/'))),
            Rule(SgmlLinkExtractor(allow=(r'/p/'), deny=(r'/sc/')), callback='parse_product', follow=True,) 
            ]

    def parse_product(self, response):
        l = XPathItemLoader(item=Product(), response=response)
        #hxc = HtmlXPathSelector(response)
        #links = hxc.select('//a/@href')
        #for link in links:
        #    yield Request(urljoin(res$ponse.url, link.extract()), callback=self.parse)
        l.add_xpath('url', '/html/head/link[@rel="canonical"]/@href')
        l.add_xpath('img', '/html/head/meta[@name="og_image"]/@content')
        l.add_xpath('name', '//h1[@itemprop="name"]/text()')
        l.add_xpath('price', '//meta[@itemprop="price"]/@content')
        l.add_xpath('desc', '/html/body//div[@id="description"]//text()')
        return l.load_item()

    def parse_review(self, response):
        l = XPathItemLoder(item=ProductReviews(), response=response)
        l.add_xpath('item_id', '')
        l.add_xpath('review', '')
        return l.load_item()
