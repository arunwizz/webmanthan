# Scrapy settings for webmanthan project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#
TELNETCONSOLE_PORT = [6023, 6032]
BOT_NAME = 'webmanthan'
ROBOTSTXT_OBEY = True
SPIDER_MODULES = ['webmanthan.spiders']
NEWSPIDER_MODULE = 'webmanthan.spiders'
DOWNLOAD_DELAY = 1
DUPEFILTER_CLASS = 'scrapy.dupefilter.RFPDupeFilter'
SPIDER_MIDDLEWARES = {'scrapy.contrib.spidermiddleware.offsite.OffsiteMiddleware': None}
ITEM_PIPELINES = ['webmanthan.pipelines.WebmanthanDBPipeline']
# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'webmanthan (+http://www.webmanthan.com)'
