# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html

from pymongo import MongoClient
import json

class WebmanthanDBPipeline(object):
    
    def __init__(self):
        db_client = MongoClient('127.0.0.1', 27017)
        self.__db = db_client.webmanthandb

    def process_item(self, item, spider):
        products = self.__db.products
        _item = dict(item)
        _obj_id = products.insert(_item)
        return item
